import os
from dotenv import load_dotenv
from fastapi.templating import Jinja2Templates

load_dotenv(".env")

DB_HOST = os.getenv("DB_HOST")
DB_NAME = os.getenv("DB_NAME")
SECRET_KEY = os.getenv("SECRET_KEY")
ALGORITHM = os.getenv("ALGORITHM", "HS256")
EXPIRE_ACCESS_TOKEN = 60  #MINUTES
X_AUTH_TOKEN = os.getenv("X_AUTH_TOKEN")

allowed_origins = os.environ.get("ALLOWED_ORIGINS", "*").split(",")
templates = Jinja2Templates(directory="src/templates")
