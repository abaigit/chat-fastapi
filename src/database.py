from motor.motor_asyncio import AsyncIOMotorClient
from src.config import DB_NAME, DB_HOST


db_client = AsyncIOMotorClient(DB_HOST)
database = db_client[DB_NAME]
