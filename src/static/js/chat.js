// temporary hard coded token. will be replaced
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRlbiIsInBhc3N3b3JkIjoiJDJiJDEyJHJIY3NlNUxDTlZJY2hadTE4bTlwb09QSnpodG84TldqdzdNTFQuZEN5NDkuZllzR1FsVkVlIiwiZXhwIjoxNjk1MjEzNjA0fQ.y1ANDQ9ct-cX1RlLobENipGpT3uvrmwYFGNWUc5S8Uc"

const fetchUserRooms = async () => {
    const response = await fetch(
        "/chat/user-rooms",
        {method: 'GET', headers: {"ACCESS-TOKEN": token}}
    );
    let rooms = []
    if (response.ok) {
        rooms = response.json();
    }
    return rooms;
}

$(document).ready(async () => {
    const rooms = await fetchUserRooms();
    console.log("User Rooms!", rooms);
    rooms.forEach((item) => {
        const option = `<option value="${item._id}">${item.name}</option>`
        $("#roomSelect").append(option);
    })
});
