import typing
from bson import ObjectId
from fastapi import Request, APIRouter, WebSocketDisconnect, Depends, status, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.websockets import WebSocket
from src.database import database
from src.config import templates
from src.auth.utils import get_user_by_token
from src.chat.connector import manager
from src.chat.dependency import get_x_auth_token
from src.chat.models import ChatRoomMixin, ChatRoom

router = APIRouter(
    prefix="/chat",
    tags=["Chat"]
)

active_rooms = {
    # 1: "websocket"
}


@router.get("/", response_class=HTMLResponse)
async def chat_page(request: Request):
    return templates.TemplateResponse("chat.html", {"request": request})


@router.get("/user-rooms", response_model=typing.List[ChatRoomMixin])
async def user_rooms(request: Request):
    access_token = request.headers.get("ACCESS-TOKEN")  # better be a dependency
    user = await get_user_by_token(access_token)
    if not user:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorized")
    user_rooms = []
    db_rooms_cursor = database.chat_rooms.find({"users.username": user.username})  # cursor instance
    async for document in db_rooms_cursor:
        room_model = ChatRoomMixin(**document)
        user_rooms.append(room_model)
    return user_rooms


@router.post("/create-room", response_model=ChatRoomMixin)
async def create_rooms(request: Request, room: ChatRoom):
    access_token = request.headers.get("ACCESS-TOKEN")  # better be a dependency
    user = await get_user_by_token(access_token)
    if not user:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authorized")
    # todo: implement creation of room
    return room


@router.websocket("/ws/{room_id}")
async def websocket_room(
        websocket: WebSocket,
        room_id: str,
        x_auth_token: str = Depends(get_x_auth_token)
):
    await manager.connect(websocket)

    try:
        while True:
            data = await websocket.receive_json()
            access_token = data.get("access_token")
            event = data.get("event")
            message = data.get("message")
            user = await get_user_by_token(access_token)

            if not access_token or not user:  # or access_token != user.get_active_token()
                raise WebSocketDisconnect(code=status.WS_1008_POLICY_VIOLATION, reason="ACCESS-TOKEN wrong or missing")

            if event == "join":
                chat_room_data = await database.chat_rooms.find_one({"_id": ObjectId(room_id)})
                if not chat_room_data:
                    raise WebSocketDisconnect(code=status.WS_1005_NO_STATUS_RCVD,
                                              reason="No such chat room")
                if room_id not in active_rooms:
                    active_rooms[room_id] = websocket

            if event == "message":
                # Broadcast the message to all users in the room
                for ws_client in active_rooms.values():
                    await ws_client.send_json({"username": user.username, "message": message})
                    # await ws_client.send_text(f"{user.username}: {message}")

    except WebSocketDisconnect:  # Handle client disconnection
        manager.disconnect(websocket)
        # await manager.broadcast(f"Client # left the chat") # todo: get user before try..except and send message only to users in the room
        del active_rooms[room_id]
