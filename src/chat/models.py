import typing
from datetime import datetime

from bson import ObjectId
from pydantic import BaseModel
from src.base_models import MBaseModel
from src.auth.models import UserModel, UserShortModel


class ChatRoom(BaseModel):
    name: str
    users: typing.List[UserShortModel]
    description: typing.Optional[typing.AnyStr] = None
    # messages: typing.List[Message] = None
    type: str  # individual or group


class ChatRoomMixin(MBaseModel, ChatRoom):
    pass


class Message(BaseModel):
    text: str
    timestamp: datetime
    user: UserModel
    chat_room: ChatRoom


