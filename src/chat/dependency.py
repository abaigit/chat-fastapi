import typing
from fastapi import Request, HTTPException, WebSocket, WebSocketException, Query, status
from src.config import X_AUTH_TOKEN


async def get_x_auth_token(
    websocket: WebSocket,
    x_auth_token: str | None = None,
):
    if x_auth_token is None or x_auth_token != X_AUTH_TOKEN:
        raise WebSocketException(code=status.WS_1008_POLICY_VIOLATION, reason="X-AUTH-TOKEN wrong or missing")
    return x_auth_token
