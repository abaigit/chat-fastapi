from fastapi import APIRouter, Request, Depends, HTTPException
from src.config import templates
from src.database import database
from src.auth.models import UserModel, UserCreateModel, UserToken, UserAuthModel
from src.auth.utils import check_password, get_or_create_token, check_user_exists, create_user


router = APIRouter(
    prefix="/auth",
    tags=["Auth"]
)


@router.post("/register", response_model=UserToken)
async def register(user_creds: UserCreateModel):
    await check_user_exists(user_creds.username)
    user = await create_user(user_creds)
    token = get_or_create_token(user)
    return token


@router.post("/login", response_model=UserToken)
async def login(creds: UserAuthModel):
    user_data = await database.users.find_one({"username": creds.username})
    user = UserModel(**user_data)
    if not user or not check_password(creds.password, user):
        raise HTTPException(status_code=403, detail="Wrong credentials")
    token = get_or_create_token(user)
    return token
