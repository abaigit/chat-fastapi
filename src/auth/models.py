import typing
from datetime import datetime
from pydantic import BaseModel, EmailStr
from src.base_models import MBaseModel


class UserToken(BaseModel):
    access_token: str
    expire_date_access_token: datetime


class UserCreateModel(BaseModel):
    username: str
    email: EmailStr
    password: str
    salt: typing.Optional[str] = None
    tokens: typing.Optional[typing.List[UserToken]] = None


class UserModel(MBaseModel):
    username: str
    email: EmailStr
    password: str
    salt: str
    tokens: typing.Optional[typing.List[UserToken]] = None

    def get_active_token(self) -> UserToken | None:
        if not self.tokens:
            return None
        token = max(self.tokens, key=lambda token: token.expire_date_access_token)
        if token.expire_date_access_token < datetime.now():
            return None
        return token

    def add_token(self, new_token: UserToken):
        self.tokens.append(new_token)


class UserAuthModel(BaseModel):
    username: str
    password: str


class UserShortModel(MBaseModel):
    username: str
    email: EmailStr
