from datetime import datetime, timedelta
import typing
import jwt
import bcrypt
from bson import ObjectId
from fastapi import HTTPException
from src.auth.models import UserModel, UserToken, UserCreateModel
from src.config import SECRET_KEY, ALGORITHM, EXPIRE_ACCESS_TOKEN
from src.database import database


def create_access_token(user: UserModel) -> UserToken:
    to_encode = {"username": user.username, "password": user.password}
    expire = datetime.now() + timedelta(minutes=EXPIRE_ACCESS_TOKEN)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    token_data = {"access_token": encoded_jwt, "expire_date_access_token": expire}
    return UserToken(**token_data)


def get_or_create_token(user: UserModel) -> UserToken:
    active_token = user.get_active_token()

    if not active_token:
        active_token = create_access_token(user)
        user.add_token(active_token)
        database.users.update_one(
            {"_id": user.id},
            {"$addToSet": {"tokens": active_token.model_dump()}}
        )
    return active_token


async def get_user_by_token(token) -> UserModel | None:
    user_data = await database.users.find_one({"tokens.access_token": token})
    user = UserModel(**user_data) if user_data else None
    return user


def hash_password(password: bytes, salt: bytes = None) -> typing.Tuple[str, str]:
    if not salt:
        salt = bcrypt.gensalt()
    salt_str = salt.decode('utf-8') if isinstance(salt, bytes) else salt
    return bcrypt.hashpw(password, salt).decode("utf-8"), salt_str


def check_password(password: str, user: UserModel) -> bool:
    input_password_hash = hash_password(password.encode('utf-8'), user.salt.encode('utf-8'))[0]
    user_password = user.password
    return input_password_hash == user_password


async def check_user_exists(username: str):
    user = await database.users.find_one({"username": username})
    if user:
        raise HTTPException(status_code=403, detail=f"{username} already in use")


async def create_user(user_creds: UserCreateModel) -> UserModel:
    hash_pw, salt = hash_password(user_creds.password.encode("utf-8"))
    user_creds.salt = salt
    user_creds.password = hash_pw
    user_creds.tokens = []
    result = await database.users.insert_one(user_creds.model_dump())
    user = await database.users.find_one({"_id": ObjectId(result.inserted_id)})
    return UserModel(**user)
