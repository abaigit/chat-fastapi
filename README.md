environment variables:
* DB_HOST
* DB_NAME
* SECRET_KEY
* ALGORITHM
* X_AUTH_TOKEN

inside `src/static/js` create `config-vars.js` and put:

    const wsHost = "https://example.com";
    const xAuthToken = "x-auth-token-example";


